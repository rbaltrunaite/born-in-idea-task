"use strict";

//let arrow = document.getElementById("left");

$(document).ready(function () {

    //Produktu karusele
    $('.owl-carousel').owlCarousel({
        loop: true,
        navigation: true,
        navText: ['<svg><path d="M 3 11 l 8.6 -8.79 a 1.32 1.32 0 0 0 0 -1.83 a 1.24 1.24 0 0 0 -1.79 0 L 0.37 10.07 A 1.31 1.31 0 0 0 0 11 v 0 a 1.28 1.28 0 0 0 0.37 0.91 l 9.47 9.69 a 1.24 1.24 0 0 0 1.79 0 a 1.32 1.32 0 0 0 0 -1.83 Z"></path></svg>',
                  '<svg><path d="M 9 11 L 0.37 19.79 a 1.32 1.32 0 0 0 0 1.83 a 1.24 1.24 0 0 0 1.79 0 l 9.47 -9.69 A 1.31 1.31 0 0 0 12 11 v 0 a 1.28 1.28 0 0 0 -0.37 -0.91 L 2.16 0.38 A 1.24 1.24 0 0 0 0.37 0.38 a 1.32 1.32 0 0 0 0 1.83 Z"></path></svg>'],
        margin: 10,
        dots: true,
        responsiveClass: true,
        responsive: {
            0: {
                items: 1
            },
            1000: {
                items: 2
            },
            1200: {
                items: 3
            }
        }
    });

    //Parodyti daugiau / maziau Instagram postu
    $(window).width();
    let width = $(window).width();

    if (width <= 870) {
        $(".show-more-button").click(function () {
            $(".inspiration-img:nth-child(n+5)").slideToggle("slow");
        });
    }

    //Mygtuko uzrasas
    (function () {
        let count = 0;
        $(".show-more-button").click(function () {
            count += 1;
            if (count % 2 === 0) {
                $(".show-more-button").text("Show more inspiration");
            } else {
                $(".show-more-button").text("Show less");
            }
        });
    })();

    $('.owl-nav').removeClass('disabled');

    $('.owl-nav').click(function(event) {
        $(this).removeClass('disabled');
    });

    //Navigacija (kad veiktu leciau, animuotai)
    document.querySelectorAll('a[href^="#"]').forEach(anchor => {
        anchor.addEventListener('click', function (e) {
            e.preventDefault();

            document.querySelector(this.getAttribute('href')).scrollIntoView({
                behavior: 'smooth'
            });
        })
    });

});
